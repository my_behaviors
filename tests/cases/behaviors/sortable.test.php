<?php 

function postCompare($a, $b) {
	if ($a['Post']['order'] == $b['Post']['order']) {
		return 0;
	}

	return ($a['Post']['order'] < $b['Post']['order']) ? -1 : 1;
}

function postTitleCompare($a, $b) {
	if ($a['Post']['title'] == $b['Post']['title']) {
		return 0;
	}

	return ($a['Post']['title'] < $b['Post']['title']) ? -1 : 1;
}

function orderPosts(&$posts, $order) {
	foreach($order as $key => $value) {
		$posts[$value - 1]['Post']['order'] = $key + 1;
	}
	usort($posts, 'postCompare');
}

class TestPost extends Model {
	var $name = 'Post';
	var $cacheSources = false;
	var $useDbConfig  = 'test_suite';
	var $validate = array(
		'title' => array('alphanumeric'),
	);
	
	var $actsAs = array('Sortable');
}

class SortableTestCase extends CakeTestCase {
	var $Post = null;
	var $fixtures = array('app.post');

	function start() {
		parent::start();
		$this->Post = new TestPost();
		$this->data = array (
			array (
				'Post' => array (
					'id' => '1',
					'title' => 'primero',
					'body' => 'uno',
					'order' => '1',
				),
			),
			array (
				'Post' => array (
					'id' => '2',
					'title' => 'segundo',
					'body' => 'dos',
					'order' => '2',
				),
			),
			array (
				'Post' => array (
					'id' => '3',
					'title' => 'tercero',
					'body' => 'tres',
					'order' => '3',
				),
			),
			array (
				'Post' => array (
					'id' => '4',
					'title' => 'cuarto',
					'body' => 'cuatro',
					'order' => '4',
				),
		));
	}

	function testPostFind() {
		$this->Post->recursive = -1;

		$expected = $this->data;

		$results = $this->Post->find('all');
		$this->assertTrue(!empty($results));
		$this->assertEqual($results, $expected);

		usort($expected, 'postTitleCompare');

		$results = $this->Post->find('all', array('order' => array('Post.title' => 'asc')));
		$this->assertEqual($results, $expected);
	}

	function testPostMoveUpAndDown() {

		$expected = $this->data;
		//             1  2  3  4
		$order = array(1, 3, 2, 4);
		orderPosts($expected, $order);

		$this->Post->recursive = -1;
		$this->Post->moveUp(3);
		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);

		$expected = $this->data;

		$this->Post->moveDown(3);
		$results = $this->Post->find('all');
		$this->assertEqual($results, $expected);
	}

	function testPostMoveUpAndDownStep() {

		$expected = $this->data;

		//             1  2  3  4
		$order = array(1, 4, 2, 3);
		orderPosts($expected, $order);

		$this->Post->recursive = -1;
		$this->Post->moveUp(4, 2);
		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);

		$expected = $this->data;

		$this->Post->moveDown(4, 2);
		$results = $this->Post->find('all');
		$this->assertEqual($results, $expected);
	}

	function testPostMoveTopAndBottom() {

		$expected = $this->data;

		//             1  2  3  4
		$order = array(3, 1, 2, 4);
		orderPosts($expected, $order);

		$this->Post->recursive = -1;
		$this->Post->moveTop(3);
		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);

		$expected = $this->data;

		//             1  2  3  4
		$order = array(1, 2, 4, 3);
		orderPosts($expected, $order);

		$this->Post->moveBottom(3);
		$results = $this->Post->find('all');
		$this->assertEqual($results, $expected);
	}

	function testCreateAtEnd() {

		$this->Post->create(array('title' => 'quinto', 'body' => 'cinco'));
		$this->assertTrue($this->Post->save());

		$expected = $this->data;
		$expected[] = array (
			'Post' => array (
				'id' => '5',
				'title' => 'quinto',
				'body' => 'cinco',
				'order' => '5',
		));

		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);
	}

	function testCreateAtPosition() {

		$this->Post->create(array('title' => 'quinto', 'body' => 'cinco', 'order' => 2));
		$this->assertTrue($this->Post->save());

		$expected = $this->data;

		$expected[] = array (
			'Post' => array (
				'id' => '5',
				'title' => 'quinto',
				'body' => 'cinco',
				'order' => '5',
		));

		//             1  2  3  4  5
		$order = array(1, 5, 2, 3, 4);
		orderPosts($expected, $order);

		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);
	}

	function testSavePosition() {

		$this->Post->id = 4;
		$this->assertTrue($this->Post->save(array('title' => 'cuarto', 'body' => 'cuatro', 'order' => 2)));

		$expected = $this->data;

		//             1  2  3  4
		$order = array(1, 4, 2, 3);
		orderPosts($expected, $order);

		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);
	}

	function testLastPosition() {

		$expected = 4;
		$results = $this->Post->lastPosition();

		$this->assertEqual($results, $expected);
	}

	function testFindByPosition() {

		$expected = $this->data[1];
		$results = $this->Post->findByPosition(2);

		$this->assertEqual($results, $expected);
	}

	function testDelete() {

		$expected = $this->data;

		//             1  2  3  4
		$order = array(1, 3, 4, 2);
		orderPosts($expected, $order);
		unset($expected[3]);

		$this->Post->del(2);
		$results = $this->Post->find('all');

		$this->assertEqual($results, $expected);
	}


}
?>