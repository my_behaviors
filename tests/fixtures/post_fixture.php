<?php 
/* SVN FILE: $Id$ */
/* Post Fixture generated on: 2008-06-17 22:06:20 : 1213751240*/

class PostFixture extends CakeTestFixture {
	var $name = 'Post';
	var $fields = array(
		'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
		'body' => array('type'=>'text', 'null' => true, 'default' => NULL),
		'order' => array('type'=>'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
	);
	var $records = array(
		array (
			'id' => '1',
			'title' => 'primero',
			'body' => 'uno',
			'order' => '1'
		),
		array (
			'id' => '2',
			'title' => 'segundo',
			'body' => 'dos',
			'order' => '2'
		),
		array (
			'id' => '3',
			'title' => 'tercero',
			'body' => 'tres',
			'order' => '3'
		),
		array (
			'id' => '4',
			'title' => 'cuarto',
			'body' => 'cuatro',
			'order' => '4'
		),
	);
}
?>